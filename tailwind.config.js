module.exports = {
  mode: 'jit',
  theme: {
    extend: {
      fontSize: {
        'xs': '.75rem',
        'sm': '.875rem',
        'tiny': '.875rem',
        'base': '1rem',
        'lg': '1.125rem',
        'xl': '1.3125rem',
        '2xl': '1.5rem',
        '3xl': '1.875rem',
        '4xl': '2.25rem',
        '5xl': '2.625rem',
        '6xl': '4rem',
        '7xl': '5rem',
        '8xl': '8.125rem',
      },
      colors: {
        transparent: 'transparent',
        current: 'currentColor',
        main: {
          light: '#81E6D9',
          DEFAULT: '#319795',
        },
        black: {
          light: '#718096',
          DEFAULT: '#4A5568',
          dark: '#2D3748'
        },
      },
      fontFamily: {
        'body': ['"Lato"', 'sans-serif']
      },
      boxShadow: {
        custom: '0 3px 6px 0 #00000029',
        custom1: '0px -1px 3px #00000033'
      },
      letterSpacing: {
        normal: 0,
        wide: '0.47px',
        wider: '0.84px',
        widest: '1.26px',
      },
      lineHeight: {
        normal: 0.85
      },
      gradientColorStops: theme => ({
        ...theme('colors'),
        'primary': '#EBF4FF',
        'secondary': '#E6FFFA',
      }),
      backgroundImage: {
        'main': "linear-gradient(141deg, #EBF4FF 0%, #E6FFFA 100%);",
        'button': "linear-gradient(95deg, #319795 0%, #3182CE 100%)",
        'wave': "linear-gradient(134deg, #E6FFFA 0%, #EBF4FF 100%)"
      },
      backgroundColor: {
        light: '#E6FFFA',
        darker: '#F7FAFC'
      },
      spacing: {
        minussm: '-8px',
        minusmd: '-16px',
        minuslg: '-24px',
        minusxl: '-48px',
      }
    }
  },
}

# tomisha

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

This is only for interview purpose. As an owner of this repo, i just do what the interviewer ask me. Thanks
